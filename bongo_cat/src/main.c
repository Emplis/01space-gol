#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <zephyr/kernel.h>

#include <zephyr/device.h>
#include <zephyr/drivers/display.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/conn.h>

#include <zephyr/logging/log.h>

#include "../../common/bluetooth/bt.h"
#include "../../common/bluetooth/conn.h"
#include "../../common/bluetooth/gatt_c.h"
#include "../../common/device_util.h"

#include "bongocat.h"

LOG_MODULE_REGISTER(playground_bcat_main, LOG_LEVEL_DBG);

#define BONGO_PEER_NAME "🪘🐱"

#define custom_service_uuid BT_UUID_DECLARE_128(BT_UUID_128_ENCODE(0xf0debc9a, 0x7856, 0x3412, 0x7856, 0x341278563412))
#define custom_characteristic_uuid                                                                                     \
    BT_UUID_DECLARE_128(BT_UUID_128_ENCODE(0xf2debc9a, 0x7856, 0x3412, 0x7856, 0x341278563412))

/* default state is paws up */
static size_t frame_idx = 3;

static uint8_t received_notification(struct bt_conn *conn,
                                     struct bt_gatt_subscribe_params *params,
                                     const void *data,
                                     uint16_t length)
{
    if (length) {
        LOG_HEXDUMP_DBG(data, length, "notification received: ");

        uint8_t bongo_state = *((uint8_t *)data);

        switch (bongo_state) {
        case 0x00:
            frame_idx = 3;
            break;
        case 0x01:
            frame_idx = 1;
            break;
        case 0x02:
            frame_idx = 2;
            break;
        case 0x03:
            frame_idx = 0;
            break;
        default:
            frame_idx = 3;
        }
    }

    return BT_GATT_ITER_CONTINUE;
}

static void connect_and_subscribe_to_bongos(void)
{
    struct bt_conn *conn;

    uint16_t svc_start_handle;
    uint16_t svc_end_handle;

    uint16_t chrc_value_handle;
    uint16_t chrc_end_handle;

    connect_to_name(BONGO_PEER_NAME);

    conn = get_conn();
    if (conn == NULL) {
        LOG_ERR("conn is null");
        k_oops();
    }

    gatt_discover_primary(conn, custom_service_uuid, &svc_start_handle, &svc_end_handle);

    gatt_discover_chrc(conn,
                       custom_characteristic_uuid,
                       svc_start_handle,
                       svc_end_handle,
                       &chrc_value_handle,
                       &chrc_end_handle);

    gatt_subscribe(conn, chrc_value_handle, received_notification);
}

int main(void)
{
    int err;

    const struct device *dev;
    struct framebuffer fb;
    uint8_t fb_mem[BC_FRAME_SIZE];

    dev = init_device();

    init_framebuffer(dev, &fb, fb_mem, sizeof(fb_mem));

    display_blanking_off(dev);

    err = init_bluetooth();
    if (err != 0) {
        LOG_ERR("Failed to initialize Bluetooth");
        k_oops();
    }

    while (true) {
        copy_to_framebuffer(&fb, bc_frames[frame_idx]);
        write_framebuffer_to_device(dev, &fb);

        if (!connected()) {
            connect_and_subscribe_to_bongos();
        }

        k_sleep(K_MSEC(50));
    }

    return 0;
}
