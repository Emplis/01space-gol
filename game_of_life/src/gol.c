#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <zephyr/kernel.h>
#include <zephyr/random/random.h>

#include "gol.h"

static void set_cell_alive(uint8_t cells[], size_t x, size_t y)
{
    size_t idx = (y * WIDTH + x) / 8;
    uint8_t bit = 1 << (x % 8);

    cells[idx] = cells[idx] | bit;
}

static void set_cell_dead(uint8_t cells[], size_t x, size_t y)
{
    size_t idx = (y * WIDTH + x) / 8;
    uint8_t bit = 1 << (x % 8);

    cells[idx] = (cells[idx] | bit) ^ bit;
}

void set_cell(uint8_t cells[], size_t x, size_t y, cell_state state)
{
    if (state == STATE_ALIVE) {
        set_cell_alive(cells, x, y);
    } else {
        set_cell_dead(cells, x, y);
    }
}

cell_state get_cell(const uint8_t cells[], size_t x, size_t y)
{
    size_t idx = (y * WIDTH + x) / 8;
    uint8_t bit = 1 << (x % 8);

    return (cells[idx] & bit) > 0 ? STATE_ALIVE : STATE_DEAD;
}

cell_state compute_next_state(const uint8_t cells[], size_t x, size_t y)
{
    if (!(x > 0 && x < WIDTH - 1 && y > 0 && y < HEIGHT - 1)) {
        return STATE_DEAD;
    }

    cell_state new_state = STATE_DEAD;
    cell_state current_state = get_cell(cells, x, y);

    uint8_t n_cell_alive_in_area = 0;

    for (size_t _y = y - 1; _y <= y + 1; _y += 1) {
        for (size_t _x = x - 1; _x <= x + 1; _x += 1) {
            n_cell_alive_in_area = n_cell_alive_in_area + get_cell(cells, _x, _y);
        }
    }

    /* 1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
     *
     * 2. Any live cell with two or three live neighbours lives on to the next generation.
     *
     * 3. Any live cell with more than three live neighbours dies, as if by overpopulation.
     *
     * 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
     *
     */
    new_state = (n_cell_alive_in_area == 3 + current_state) | (n_cell_alive_in_area == 3);

    return new_state > 0 ? STATE_ALIVE : STATE_DEAD;
}

void next_state(const uint8_t cells[], uint8_t next_cells[])
{
    for (size_t y = 0; y < HEIGHT; y += 1) {
        for (size_t x = 0; x < WIDTH; x += 1) {
            cell_state new_state = compute_next_state(cells, x, y);

            set_cell(next_cells, x, y, new_state);
        }
    }
}

void set_random_cells(uint8_t cells[])
{
    for (size_t y = 0; y < HEIGHT; y += 1) {
        for (size_t x = 0; x < WIDTH; x += 1) {
            uint8_t random_state = sys_rand8_get() % 2;
            set_cell(cells, x, y, random_state);
        }
    }
}
