#ifndef GOL_H_
#define GOL_H_

#include <stdint.h>
#include <unistd.h>

#define WIDTH          72
#define HEIGHT         40
#define CELLS_BUF_SIZE ((WIDTH * HEIGHT) / 8)

enum cell_state {
    STATE_DEAD = 0x00,
    STATE_ALIVE = 0x01,
};

typedef enum cell_state cell_state;

cell_state get_cell(const uint8_t cells[], size_t x, size_t y);
void set_cell(uint8_t cells[], size_t x, size_t y, cell_state state);

void next_state(const uint8_t cells[], uint8_t next_cells[]);

void set_random_cells(uint8_t cells[]);

#endif
