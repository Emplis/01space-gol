#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <zephyr/device.h>
#include <zephyr/display/cfb.h>
#include <zephyr/drivers/display.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>
#include <zephyr/sys/printk.h>

#include "../../common/device_util.h"
#include "gol.h"

LOG_MODULE_REGISTER(playground_gol_main, LOG_LEVEL_DBG);

static void copy_cells_to_framebuffer(uint8_t cells[], struct framebuffer *fb)
{
    clear_framebuffer(fb);
    for (size_t y = 0; y < HEIGHT; y += 1) {
        for (size_t x = 0; x < WIDTH; x += 1) {
            if (get_cell(cells, x, y) == STATE_ALIVE) {
                set_pixel(fb, x, y);
            }
        }
    }
}

int main(void)
{
    const struct device *dev;
    struct framebuffer fb;
    uint8_t fb_mem[CELLS_BUF_SIZE];

    dev = init_device();

    init_framebuffer(dev, &fb, fb_mem, sizeof(fb_mem));

    display_blanking_off(dev);

    uint8_t cells[CELLS_BUF_SIZE] = {0};
    uint8_t next_cells[CELLS_BUF_SIZE] = {0};

    uint8_t *p_cells = cells;
    uint8_t *p_next_cells = next_cells;
    uint8_t *p_tmp_cells = NULL;

    set_random_cells(cells);

    while (true) {
        copy_cells_to_framebuffer(p_cells, &fb);

        write_framebuffer_to_device(dev, &fb);

        next_state(p_cells, p_next_cells);

        p_tmp_cells = p_cells;
        p_cells = p_next_cells;
        p_next_cells = p_tmp_cells;

        k_sleep(K_MSEC(100));
    }

    return 0;
}
