# 01Space/ESP32-C3 Playground

Collection of small Zephyr applications to play with the tiny [01Space/ESP32-C3
board](https://github.com/01Space/ESP32-C3-0.42LCD/). Find short descriptions of the [applications](#applications)
bellow.

# Configuring the project ⚙️

```bash
# fetch this repository repository and init Zephyr
west init -m https://gitlab.com/Emplis/01space-esp32c3-playground
west update
# fetch espressif blobs
west blobs fetch hal_espressif
# go to one of the application folder
cd ...
# build the application
west build -b esp32c3_042_oled
# flash the application
west flash --esp-device /dev/ttyACMxx
# access device logs
west espressif monitor -p /dev/ttyACMxx
```

Alternatively, it is possible to run the applications on the `native_sim` board. Zephyr will use a
driver based on [SDL2](https://www.libsdl.org/) to emulate a display. Find more information on the
official [Zephyr
documentation](https://docs.zephyrproject.org/latest/boards/native/native_sim/doc/index.html#nsim-per-disp-sdl).

```bash
# build the application for the `native_sim` board
# make sure to delete the build folder if you previously builded for esp32c3_042_oled
# or do a pristine build with `-p`
west build -b native_sim
# run the application
./build/zephyr/zephyr.exe
```

# Applications

- [Game of Life 🧬](#game-of-life-)
- [Nyancat 🌈](#nyancat-)
- [Bongo Cat 🪘](#bongo-cat-)

## Game of Life 🧬

[./game_of_life/](./game_of_life/)

## Nyancat 🌈

[./nyancat/](./nyancat/)

## Bongo Cat 🪘

[./bongo_cat/](./bongo_cat/)

![Bongo Cat demo](resources/bongocat_demo.webm)

To run this application natively you must specify the HCI device with `--bt-dev=xxxx`. Find more
information on the [Zephyr
documentation](https://docs.zephyrproject.org/latest/boards/native/native_sim/doc/index.html#nsim-bt-host-cont).
The following commands is an example, it may not work the exact same way for you.

```bash
# build the application
west build -b native_sim
# power down `hci0` device
sudo hciconfig hci0 down
# run the application
sudo ./build/zephyr/zephyr.exe --bt-dev=hci0
```

### Bongos 🪘
