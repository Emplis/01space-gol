#ifndef DEVICE_UTIL_H_
#define DEVICE_UTIL_H_

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <zephyr/device.h>
#include <zephyr/drivers/display.h>
#include <zephyr/kernel.h>

struct framebuffer {
    /** Pointer to a buffer in RAM */
    uint8_t *buf;

    /** Size of the framebuffer */
    uint32_t size;

    enum display_pixel_format pixel_format;

    enum display_screen_info screen_info;

    /** Resolution of a framebuffer in pixels in X direction */
    uint16_t x_res;

    /** Resolution of a framebuffer in pixels in Y direction */
    uint16_t y_res;

    /** Number of pixels per tile, typically 8 */
    uint8_t ppt;

    bool inverted;
};

const struct device *init_device(void);
void clear_framebuffer(const struct framebuffer *fb);
void set_pixel(struct framebuffer *fb, int16_t x, int16_t y);
void copy_to_framebuffer(const struct framebuffer *fb, const uint8_t *buf);
void write_framebuffer_to_device(const struct device *dev, const struct framebuffer *fb);
void init_framebuffer(const struct device *dev, struct framebuffer *fb, uint8_t *fb_mem, size_t fb_mem_size);

#endif /* DEVICE_UTIL_H_ */
