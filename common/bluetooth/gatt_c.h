#ifndef COMMON_BLUETOOTH_GATT_C_H_
#define COMMON_BLUETOOTH_GATT_C_H_

#include <stdbool.h>
#include <stdint.h>

#include <zephyr/kernel.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/uuid.h>

int gatt_discover_primary(struct bt_conn *conn,
                          const struct bt_uuid *svc_uuid,
                          uint16_t *svc_start_handle,
                          uint16_t *svc_end_handle);
int gatt_discover_chrc(struct bt_conn *conn,
                       const struct bt_uuid *chrc_uuid,
                       uint16_t svc_start_handle,
                       uint16_t svc_end_handle,
                       uint16_t *chrc_value_handle,
                       uint16_t *chrc_end_handle);
int gatt_subscribe(struct bt_conn *conn, uint16_t chrc_handle, bt_gatt_notify_func_t cb);

#endif /* COMMON_BLUETOOTH_GATT_C_H_ */
