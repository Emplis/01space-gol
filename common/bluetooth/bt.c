#include <zephyr/kernel.h>

#include <zephyr/logging/log.h>

#include <zephyr/bluetooth/addr.h>
#include <zephyr/bluetooth/bluetooth.h>

#include "conn.h"

LOG_MODULE_REGISTER(playground_common_bluetooth_bt, LOG_LEVEL_DBG);

int init_bluetooth(void)
{
    int err;

    err = bt_enable(NULL);
    if (err) {
        LOG_ERR("Bluetooth init failed (err %d)", err);
        return -1;
    }

    LOG_DBG("Bluetooth initialized");

    err = bt_unpair(BT_ID_DEFAULT, BT_ADDR_LE_ANY);
    if (err != 0) {
        LOG_ERR("Unpairing failed (err %d)", err);
    }

    err = init_conn_cb();
    if (err != 0) {
        LOG_ERR("Failed to initialize connection callbacks.");
    }

    return 0;
}
