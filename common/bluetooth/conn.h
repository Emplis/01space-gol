#ifndef COMMON_BLUETOOTH_CONN_H_
#define COMMON_BLUETOOTH_CONN_H_

#include <zephyr/kernel.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/conn.h>

int init_conn_cb(void);
int connect_to_name(const char *name);
bool connected(void);
struct bt_conn *get_conn(void);

#endif /* COMMON_BLUETOOTH_CONN_H_ */
