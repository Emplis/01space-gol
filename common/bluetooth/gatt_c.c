#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include <zephyr/bluetooth/addr.h>
#include <zephyr/bluetooth/att.h>
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/uuid.h>

#include "gatt_c.h"

LOG_MODULE_REGISTER(playground_common_bluetooth_gatt_c, LOG_LEVEL_DBG);

struct gatt_c_discover_primary_ctx {
    const struct bt_uuid *svc_uuid;
    struct bt_gatt_discover_params params;

    bool svc_found;
    uint16_t start_handle;
    uint16_t end_handle;

    struct k_sem sem;
};

struct gatt_c_discover_chrc_ctx {
    struct bt_gatt_discover_params params;

    bool chrc_found;
    uint16_t value_handle;
    uint16_t end_handle;

    struct k_sem sem;
};

struct gatt_c_subscribe_ctx {
    struct bt_gatt_subscribe_params params;
    struct bt_gatt_discover_params ccc_discover_params;

    struct k_sem sem;
};

static struct gatt_c_discover_primary_ctx discover_primary_ctx;
static struct gatt_c_discover_chrc_ctx discover_chrc_ctx;
static struct gatt_c_subscribe_ctx subscribe_ctx;

static uint8_t gatt_discover_primary_cb(struct bt_conn *conn,
                                        const struct bt_gatt_attr *attr,
                                        struct bt_gatt_discover_params *params)
{
    if (attr == NULL) {
        (void)memset(params, 0, sizeof(*params));

        discover_primary_ctx.svc_found = false;

        k_sem_give(&discover_primary_ctx.sem);

        return BT_GATT_ITER_STOP;
    }

    LOG_INF("[ATTRIBUTE] handle %u", attr->handle);

    discover_primary_ctx.svc_found = true;
    discover_primary_ctx.start_handle = attr->handle;
    discover_primary_ctx.end_handle = ((struct bt_gatt_service_val *)attr->user_data)->end_handle;

    k_sem_give(&discover_primary_ctx.sem);

    return BT_GATT_ITER_STOP;
}

int gatt_discover_primary(struct bt_conn *conn,
                          const struct bt_uuid *svc_uuid,
                          uint16_t *svc_start_handle,
                          uint16_t *svc_end_handle)
{
    int err;

    k_sem_init(&discover_primary_ctx.sem, 0, 1);

    discover_primary_ctx.svc_uuid = svc_uuid;
    discover_primary_ctx.svc_found = false;

    discover_primary_ctx.params.uuid = svc_uuid;
    discover_primary_ctx.params.func = gatt_discover_primary_cb;
    discover_primary_ctx.params.start_handle = BT_ATT_FIRST_ATTRIBUTE_HANDLE;
    discover_primary_ctx.params.end_handle = BT_ATT_LAST_ATTRIBUTE_HANDLE;
    discover_primary_ctx.params.type = BT_GATT_DISCOVER_PRIMARY;

    err = bt_gatt_discover(conn, &discover_primary_ctx.params);
    if (err != 0) {
        LOG_ERR("GATT discover failed...");
    }

    k_sem_take(&discover_primary_ctx.sem, K_FOREVER);
    LOG_DBG("Discover of primary service done.");

    if (!discover_primary_ctx.svc_found) {
        LOG_WRN("Service not found.");
        return -1;
    }

    *svc_start_handle = discover_primary_ctx.start_handle;
    *svc_end_handle = discover_primary_ctx.end_handle;

    return 0;
}

static uint8_t gatt_discover_chrc_cb(struct bt_conn *conn,
                                     const struct bt_gatt_attr *attr,
                                     struct bt_gatt_discover_params *params)
{
    bool read_more = false;

    if (!discover_chrc_ctx.chrc_found) {
        if (attr != NULL) {
            LOG_INF("[ATTRIBUTE] handle %u", attr->handle);

            discover_chrc_ctx.chrc_found = true;
            discover_chrc_ctx.value_handle = ((struct bt_gatt_chrc *)attr->user_data)->value_handle;

            read_more = true;
        }
    } else {
        if (attr != NULL) {
            discover_chrc_ctx.end_handle = (attr->handle - 1);
        }
    }

    if (!read_more) {
        k_sem_give(&discover_chrc_ctx.sem);
    }

    return read_more ? BT_GATT_ITER_CONTINUE : BT_GATT_ITER_STOP;
}

int gatt_discover_chrc(struct bt_conn *conn,
                       const struct bt_uuid *chrc_uuid,
                       uint16_t svc_start_handle,
                       uint16_t svc_end_handle,
                       uint16_t *chrc_value_handle,
                       uint16_t *chrc_end_handle)
{
    int err;

    discover_chrc_ctx.chrc_found = false;

    k_sem_init(&discover_chrc_ctx.sem, 0, 1);

    discover_chrc_ctx.params.uuid = chrc_uuid;
    discover_chrc_ctx.params.func = gatt_discover_chrc_cb;
    discover_chrc_ctx.params.start_handle = svc_start_handle;
    discover_chrc_ctx.params.end_handle = svc_end_handle;
    discover_chrc_ctx.params.type = BT_GATT_DISCOVER_CHARACTERISTIC;

    err = bt_gatt_discover(conn, &discover_chrc_ctx.params);
    if (err != 0) {
        LOG_ERR("Discovery of characteristic failed...");
        return -1;
    }

    k_sem_take(&discover_chrc_ctx.sem, K_FOREVER);
    LOG_DBG("Discover of characteristic done.");

    if (!discover_chrc_ctx.chrc_found) {
        LOG_WRN("Characteristic not found.");
        return -1;
    }

    *chrc_value_handle = discover_chrc_ctx.value_handle;
    *chrc_end_handle = discover_chrc_ctx.end_handle;

    return 0;
}

static void gatt_subscribe_cb(struct bt_conn *conn, uint8_t err, struct bt_gatt_subscribe_params *params)
{
    if (err != 0) {
        LOG_ERR("Subscribe failed (err %d)", err);
        k_oops();
    }

    if (params == NULL) {
        LOG_ERR("params is null");
        k_oops();
    }

    LOG_DBG("Subscribed to handle 0x%04x", params->value_handle);
    k_sem_give(&subscribe_ctx.sem);
}

int gatt_subscribe(struct bt_conn *conn, uint16_t chrc_handle, bt_gatt_notify_func_t notify_cb)
{
    int err;

    k_sem_init(&subscribe_ctx.sem, 0, 1);

    subscribe_ctx.params.notify = notify_cb;
    subscribe_ctx.params.subscribe = gatt_subscribe_cb;
    subscribe_ctx.params.value = BT_GATT_CCC_NOTIFY;
    subscribe_ctx.params.value_handle = chrc_handle;

    /* set-up auto-discovery of the CCC handle */
    subscribe_ctx.params.ccc_handle = 0;
    subscribe_ctx.params.disc_params = &subscribe_ctx.ccc_discover_params;
    subscribe_ctx.params.end_handle = BT_ATT_LAST_ATTRIBUTE_HANDLE;

    err = bt_gatt_subscribe(conn, &subscribe_ctx.params);
    if (err != 0) {
        LOG_ERR("Failed to subscribe to characteristic.");
        return -1;
    }

    k_sem_take(&subscribe_ctx.sem, K_FOREVER);
    LOG_DBG("Successfully subscribed to characteristic.");

    return 0;
}
