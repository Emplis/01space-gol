#include <string.h>
#include <zephyr/kernel.h>

#include <zephyr/logging/log.h>
#include <zephyr/net/buf.h>
#include <zephyr/sys/printk.h>

#include <zephyr/bluetooth/addr.h>
#include <zephyr/bluetooth/att.h>
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/conn.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/uuid.h>

#include "conn.h"

LOG_MODULE_REGISTER(playground_common_bluetooth_conn, LOG_LEVEL_DBG);

struct conn_ctx {
    const char *peer_name;

    struct bt_conn *conn;
    struct bt_conn_cb conn_cb;

    struct k_sem sem;
};

static struct conn_ctx ctx;

static bool data_parse_cb(struct bt_data *data, void *user_data)
{
    bool *is_peer = (bool *)user_data;
    const char *peer_name = ctx.peer_name;

    if (data->type == BT_DATA_NAME_COMPLETE) {
        LOG_DBG("Name: %.*s (size: %d)", data->data_len, data->data, data->data_len);
        LOG_HEXDUMP_DBG(data->data, data->data_len, "ad name: ");
        LOG_HEXDUMP_DBG(peer_name, strlen(peer_name), "peer name: ");

        // ad name is not null terminated
        if (data->data_len == strlen(peer_name) && memcmp(data->data, peer_name, strlen(peer_name)) == 0) {
            LOG_DBG("=== PEER FOUND ===");
            *is_peer = true;
        } else {
            *is_peer = false;
        }
    } else {
        // LOG_DBG("len : %u", data->data_len);
        // LOG_DBG("type: 0x%02x", data->type);
        // LOG_HEXDUMP_DBG(data->data, data->data_len, "data:");
    }

    return true;
}

static void device_found(const bt_addr_le_t *addr, int8_t rssi, uint8_t type, struct net_buf_simple *ad)
{
    int err;
    bool is_peer;
    char addr_str[BT_ADDR_LE_STR_LEN];

    if (ctx.conn != NULL) {
        return;
    }

    bt_addr_le_to_str(addr, addr_str, sizeof(addr_str));

    LOG_DBG("Device found: %s (RSSI %d)", addr_str, rssi);

    is_peer = false;
    bt_data_parse(ad, data_parse_cb, &is_peer);

    if (is_peer) {
        err = bt_le_scan_stop();
        if (err) {
            LOG_DBG("Failed to stop scanner (err %d)", err);
            return;
        }

        err = bt_conn_le_create(addr, BT_CONN_LE_CREATE_CONN, BT_LE_CONN_PARAM_DEFAULT, &ctx.conn);
        if (err) {
            LOG_DBG("Failed to connect to %s (err %d)", addr_str, err);
            return;
        }
    }
}

static int start_scan(void)
{
    int err;

    err = bt_le_scan_start(BT_LE_SCAN_PASSIVE, device_found);
    if (err != 0) {
        LOG_ERR("Scanning failed to start (err %d)", err);
        return -1;
    }

    LOG_DBG("Scanning started.");

    return 0;
}

int connect_to_name(const char *name)
{
    int err;

    ctx.peer_name = name;

    k_sem_init(&ctx.sem, 0, 1);

    err = start_scan();
    if (err != 0) {
        return err;
    }

    LOG_DBG("Waiting for connection...");
    k_sem_take(&ctx.sem, K_FOREVER);

    return 0;
}

bool connected(void)
{
    return ctx.conn != NULL;
}

struct bt_conn *get_conn(void)
{
    return ctx.conn;
}

static void connected_cb(struct bt_conn *conn, uint8_t conn_err)
{
    char addr[BT_ADDR_LE_STR_LEN];

    bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

    if (conn_err) {
        LOG_ERR("Failed to connect to %s (err %u)", addr, conn_err);

        bt_conn_unref(ctx.conn);
        ctx.conn = NULL;

        start_scan();

        return;
    }

    LOG_DBG("Connected to: %s", addr);

    k_sem_give(&ctx.sem);
}

static void disconnected_cb(struct bt_conn *conn, uint8_t reason)
{
    char addr[BT_ADDR_LE_STR_LEN];

    bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));

    LOG_DBG("Disconnected: %s (reason 0x%02x)", addr, reason);

    if (ctx.conn != conn) {
        return;
    }

    bt_conn_unref(ctx.conn);
    ctx.conn = NULL;
}

int init_conn_cb(void)
{
    ctx.conn_cb.connected = connected_cb;
    ctx.conn_cb.disconnected = disconnected_cb;

    bt_conn_cb_register(&ctx.conn_cb);

    return 0;
}
