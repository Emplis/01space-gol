#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <zephyr/device.h>
#include <zephyr/drivers/display.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "device_util.h"

LOG_MODULE_REGISTER(playground_device_util, LOG_LEVEL_DBG);

static inline uint8_t byte_reverse(uint8_t b)
{
    b = (b & 0xf0) >> 4 | (b & 0x0f) << 4;
    b = (b & 0xcc) >> 2 | (b & 0x33) << 2;
    b = (b & 0xaa) >> 1 | (b & 0x55) << 1;
    return b;
}

static void reverse_framebuffer_bytes(const struct framebuffer *fb)
{
    for (size_t i = 0; i < fb->size; i += 1) {
        fb->buf[i] = byte_reverse(fb->buf[i]);
    }
}

static void invert_framebuffer(const struct framebuffer *fb)
{
    for (size_t i = 0; i < fb->size; i += 1) {
        fb->buf[i] = ~fb->buf[i];
    }
}

void clear_framebuffer(const struct framebuffer *fb)
{
    if (fb == NULL) {
        LOG_DBG("Framebuffer is NULL");
        k_oops();
    }

    memset(fb->buf, 0, fb->size);
}

void copy_to_framebuffer(const struct framebuffer *fb, const uint8_t *buf)
{
    memcpy(fb->buf, buf, fb->size);
}

void write_framebuffer_to_device(const struct device *dev, const struct framebuffer *fb)
{
    int err;
    struct display_capabilities cfg;
    struct display_buffer_descriptor desc;

    const struct display_driver_api *api = dev->api;

    api->get_capabilities(dev, &cfg);

    const bool need_reverse = (cfg.screen_info & SCREEN_INFO_MONO_MSB_FIRST) != 0;
    const bool need_invert = !(fb->pixel_format & PIXEL_FORMAT_MONO10) != !(fb->inverted);

    if (!fb || !fb->buf) {
        k_oops();
    }

    desc.buf_size = fb->size;
    desc.width = fb->x_res;
    desc.height = fb->y_res;
    desc.pitch = fb->x_res;

    if (need_reverse) {
        reverse_framebuffer_bytes(fb);
    }

    if (need_invert) {
        invert_framebuffer(fb);
    }

    err = api->write(dev, 0, 0, &desc, fb->buf);
    if (err != 0) {
        k_oops();
    }

    if (need_invert) {
        invert_framebuffer(fb);
    }

    if (need_reverse) {
        reverse_framebuffer_bytes(fb);
    }
}

void set_pixel(struct framebuffer *fb, int16_t x, int16_t y)
{
    const size_t index = ((y / 8) * fb->x_res);
    uint8_t m = BIT(y % 8);

    if (x < 0 || x >= fb->x_res) {
        return;
    }

    if (y < 0 || y >= fb->y_res) {
        return;
    }

    fb->buf[index + x] |= m;
}

void init_framebuffer(const struct device *dev, struct framebuffer *fb, uint8_t *fb_mem, size_t fb_mem_size)
{
    if (dev == NULL) {
        LOG_ERR("device is null");
        k_oops();
    }

    struct display_capabilities cfg;
    const struct display_driver_api *api = dev->api;

    if (fb == NULL) {
        LOG_ERR("fb is NULL");
        k_oops();
    }

    api->get_capabilities(dev, &cfg);

    fb->x_res = cfg.x_resolution;
    fb->y_res = cfg.y_resolution;
    fb->ppt = 8U;
    fb->pixel_format = cfg.current_pixel_format;
    fb->screen_info = cfg.screen_info;
    fb->inverted = false;

    fb->size = fb->x_res * fb->y_res / fb->ppt;

    LOG_DBG("Minimum framebuffer size: %d", fb->size);
    LOG_DBG("User given buffer size: %d", fb_mem_size);

    if (fb_mem == NULL || fb->size > fb_mem_size) {
        LOG_ERR("fb_mem is null or computed fb size is less than the given buf size");
        k_oops();
    }

    fb->buf = fb_mem;
    memset(fb->buf, 0, fb->size);

    write_framebuffer_to_device(dev, fb);
}

const struct device *init_device(void)
{
    const struct device *dev;

    dev = DEVICE_DT_GET(DT_CHOSEN(zephyr_display));

    if (!device_is_ready(dev)) {
        LOG_ERR("Device %s not ready", dev->name);
        k_oops();
    }

    if (display_set_pixel_format(dev, PIXEL_FORMAT_MONO10) != 0) {
        if (display_set_pixel_format(dev, PIXEL_FORMAT_MONO01) != 0) {
            LOG_ERR("Failed to set required pixel format");
            k_oops();
        }
    }

    LOG_DBG("Initialized %s", dev->name);

    return dev;
}
