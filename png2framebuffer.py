import argparse
import sys
from pathlib import Path

from PIL import Image


class FrameBuffer:
    def __init__(self, width: int, height: int, compressed: bool):
        self.width = width
        self.height = height
        self.compressed = compressed

        if self.compressed:
            self.buf = [0] * ((self.width * self.height) // 8)
        else:
            self.buf = [0] * (width * height)

    def __str__(self):
        padding = 4

        return " ".join([f"{val:#0{padding}x}," for val in self.buf])

    def get_buf_size(self):
        return len(self.buf)

    def get_width(self) -> int:
        return self.width

    def get_height(self) -> int:
        return self.height

    def __set_pixel(self, x: int, y: int):
        self.buf[y * self.width + x] = 1

    def __set_compressed_pixel(self, x: int, y: int):
        idx = (y // 8) * self.width
        m = 1 << (y % 8)

        self.buf[idx + x] = self.buf[idx + x] | m

    def set_pixel(self, x: int, y: int):
        if self.compressed:
            self.__set_compressed_pixel(x, y)
        else:
            self.__set_pixel(x, y)

    def __get_pixel(self, x: int, y: int) -> int:
        return self.buf[y * self.width + x]

    def __get_compressed_pixel(self, x: int, y: int) -> int:
        idx = (y // 8) * self.width
        m = 1 << (y % 8)

        if self.buf[idx + x] & m != 0:
            return 1
        else:
            return 0

    def get_pixel(self, x: int, y: int):
        if self.compressed:
            return self.__get_compressed_pixel(x, y)
        else:
            return self.__get_pixel(x, y)

    def __invert_fb(self):
        for i in range(0, self.get_buf_size()):
            self.buf[i] = 0x00 if self.buf[i] == 0x01 else 0x01

    def __invert_compressed_fb(self):
        for i in range(0, self.get_buf_size()):
            self.buf[i] = self.buf[i] ^ 0xFF

    def invert(self):
        if self.compressed:
            self.__invert_compressed_fb()
        else:
            self.__invert_fb()


def get_framebuffer_from_png(img_path: Path) -> FrameBuffer:
    img = Image.open(img_path)

    width, height = img.size

    if not ((width == 72 and height == 40) or (width == 40 and height == 72)):
        print(
            f"ERROR: '{img_path}' does not have the correct size. {width}x{height} when it should be 72x40.",
            file=sys.stderr,
        )
        sys.exit(1)

    fb = FrameBuffer(width, height, compressed=False)

    for y in range(height):
        for x in range(width):
            r, g, b, a = img.load()[x, y]

            if a > 128 and not (r == 255 and g == 255 and b == 255):
                # if opacity is superior to 50% or the pixel is not white, write that pixel
                fb.set_pixel(x, y)

    return fb


def compress_framebuffer(fb: FrameBuffer) -> FrameBuffer:
    width = fb.get_width()
    height = fb.get_height()
    compressed_fb = FrameBuffer(width, height, compressed=True)

    for y in range(height):
        for x in range(width):
            if fb.get_pixel(x, y) == 1:
                compressed_fb.set_pixel(x, y)

    return compressed_fb


def get_c_code(framebuffers: list, prefix: str = ""):
    nb_frames = len(framebuffers)

    # the following data must be the same for all frames
    frame_size = framebuffers[0].get_buf_size()
    frame_width = framebuffers[0].get_width()
    frame_height = framebuffers[0].get_height()

    lower_prefix = prefix.lower()
    upper_prefix = prefix.upper()

    c_code = f"#define {upper_prefix}FRAME_SIZE\t{frame_size}\n"
    c_code += f"#define {upper_prefix}FRAME_COUNT\t{nb_frames}\n"
    c_code += f"#define {upper_prefix}FRAME_WIDTH\t{frame_width}\n"
    c_code += f"#define {upper_prefix}FRAME_HEIGHT\t{frame_height}\n"
    c_code += "\n"
    c_code += f"static const uint8_t {lower_prefix}frames[{upper_prefix}FRAME_COUNT][{upper_prefix}FRAME_SIZE] = {{\n"

    for fb in framebuffers:
        c_code += f"\t{{{fb}}},\n"

    c_code += "};\n"

    return c_code


def parse_args() -> dict:
    def valid_path(path: str) -> Path:
        path = Path(path)
        if not path.exists():
            raise argparse.ArgumentTypeError(f"'{path.resolve()}' does not exist.")

        return path

    parser = argparse.ArgumentParser(description="Convert images to C array.")
    parser.add_argument(
        "images",
        metavar="IMG",
        type=valid_path,
        nargs="+",
        help="List of images for which to generate an array.",
    )
    parser.add_argument(
        "--compress",
        "-c",
        action="store_true",
        help="Use one bit to store a pixel instead of using an octet, useful to save memory. Warning: the current implementation is driver specific.",
    )
    parser.add_argument(
        "--invert",
        "-i",
        action="store_true",
        help="By default, the script set the bits to 1 to write a pixel. Use that option to use 0 instead (and 1 will be used for non-set pixels).",
    )
    parser.add_argument(
        "--prefix",
        "-p",
        type=str,
        default="",
        help="String to prepend to the variable in the generated C code.",
    )

    args = parser.parse_args()

    return args


def main():
    args = parse_args()

    framebuffers = []

    for img in args.images:
        fb = get_framebuffer_from_png(img)

        if args.compress:
            fb = compress_framebuffer(fb)

        if args.invert:
            fb.invert()

        framebuffers.append(fb)

    c_code = get_c_code(framebuffers, args.prefix)

    print(c_code)


if __name__ == "__main__":
    main()
