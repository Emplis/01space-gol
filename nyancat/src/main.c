#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <zephyr/device.h>
#include <zephyr/drivers/display.h>
#include <zephyr/kernel.h>
#include <zephyr/logging/log.h>

#include "../../common/device_util.h"
#include "nyancat.h"

LOG_MODULE_REGISTER(playground_nc_main, LOG_LEVEL_DBG);

int main(void)
{
    const struct device *dev;
    struct framebuffer fb;
    uint8_t fb_mem[NC_FRAME_SIZE];

    dev = init_device();

    init_framebuffer(dev, &fb, fb_mem, sizeof(fb_mem));

    display_blanking_off(dev);

    uint8_t frame_idx = 0;

    while (true) {
        copy_to_framebuffer(&fb, nc_frames[frame_idx]);
        write_framebuffer_to_device(dev, &fb);

        frame_idx = (frame_idx + 1) % NC_FRAME_COUNT;

        k_sleep(K_MSEC(100));
    }

    return 0;
}
